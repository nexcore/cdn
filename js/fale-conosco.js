$( document ).ready(function() {
        var span = '<span class="span_error"></span>';
        $('#idfale_conosco_telefone').parents('.form-group').append(span);
        $('#idfale_conosco_telefone_clicktocall').parents('.form-group').append(span);
        var SPMaskBehavior = function (val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
    },spOptions = {
        onKeyPress: function(val, e, field, options) {
            field.mask(SPMaskBehavior.apply({}, arguments), options);
        }
    }
    $('#idfale_conosco_telefone').mask(SPMaskBehavior, spOptions);
    $('#idfale_conosco_telefone_clicktocall').mask(SPMaskBehavior, spOptions);
    var campos_quantidade_nchat = $('.nchat_campos').find('.form-group').length;
    if (campos_quantidade_nchat > 6){
        $('.form-group').removeClass('col-lg-12 col-md-12').addClass('col-lg-6 col-md-6');
        $('.chat_nexcore').addClass('col-lg-offset-5 col-md-offset-5 col-lg-5 col-md-5');
    } 
    var height_fale_conosco = $('.chat_nexcore').height();
    var total_bottom = height_fale_conosco - 40;
    document.getElementById("chat_nexcore").style.bottom = "-" + total_bottom + "px";
    function calcula_bottom () {
        $('.chat_nexcore').toggleClass('chat_nexcore_toggle');
        $('.header_fale_conosco').toggleClass('header_fale_conosco_toggle');
        $('.fale_conosco').toggleClass('fale_conosco_toggle');
        $('.chevron_up_down .glyphicon').toggleClass('glyphicon-chevron-up glyphicon-chevron-down');
        if($("#chat_nexcore").hasClass("chat_nexcore_toggle")){
            document.getElementById("chat_nexcore").style.bottom = "0px";
        } else {
            var height_fale_conosco = $('.chat_nexcore').height();
            var total_bottom = height_fale_conosco - 40;
            document.getElementById("chat_nexcore").style.bottom = "-" + total_bottom + "px";
        }
    }
    $('.header_fale_conosco').on('click', function () {
        calcula_bottom();
    });
    $('.email').on('click', function(){
        var bottom = $("#chat_nexcore").css("bottom");
        console.log(bottom);
        $('.icone_nex i').removeClass('icon-nex-nchat glyphicon glyphicon-earphone').addClass('glyphicon glyphicon-envelope');
        if(bottom != 0){
            document.getElementById("chat_nexcore").style.bottom = "0px";
            $('.chat_nexcore').addClass('chat_nexcore_toggle');
            $('.fale_conosco').addClass('fale_conosco_toggle');
            $('.header_fale_conosco').addClass('header_fale_conosco_toggle');
            $('.clicktocall_campos').hide("slow");
            $('.nchat_campos').hide("slow");
            $('.email_campos').show("slow");
            $('.chevron_up_down .glyphicon').addClass('glyphicon-chevron-up').removeClass('glyphicon-chevron-down');
        } else {
            $('.clicktocall_campos').hide("slow");
            $('.nchat_campos').hide("slow");
            $('.email_campos').show("slow");
        }
    });
    $('.nchat').on('click', function(){
        $('#idfale_conosco_telefone').mask(SPMaskBehavior, spOptions);
        var bottom = $("#chat_nexcore").css("bottom");
        $('.icone_nex i').removeClass('glyphicon glyphicon-envelope glyphicon-earphone').addClass('icon-nex-nchat');
        if(bottom != 0){
            document.getElementById("chat_nexcore").style.bottom = "0px";
            $('.chat_nexcore').addClass('chat_nexcore_toggle');
            $('.fale_conosco').addClass('fale_conosco_toggle');
            $('.header_fale_conosco').addClass('header_fale_conosco_toggle');
            $('.clicktocall_campos').hide();
            $('.email_campos').hide("slow");
            $('.nchat_campos').show("slow");
            $('.chevron_up_down .glyphicon').addClass('glyphicon-chevron-up').removeClass('glyphicon-chevron-down');
        } else {
            $('.clicktocall_campos').hide("slow");
            $('.email_campos').hide("slow");
            $('.nchat_campos').show("slow");
        }
    });
    $('.clicktocall').on('click', function(){
        $('#idfale_conosco_telefone_clicktocall').mask(SPMaskBehavior, spOptions);
        var bottom = $("#chat_nexcore").css("bottom");
        $('.icone_nex i').removeClass('glyphicon glyphicon-envelope icon-nex-nchat').addClass('glyphicon glyphicon-earphone');
        if(bottom != 0){
            document.getElementById("chat_nexcore").style.bottom = "0px";
            $('.chat_nexcore').addClass('chat_nexcore_toggle');
            $('.fale_conosco').addClass('fale_conosco_toggle');
            $('.header_fale_conosco').addClass('header_fale_conosco_toggle');
            $('.nchat_campos').hide("slow");
            $('.email_campos').hide("slow");
            $('.clicktocall_campos').show("slow");
            $('.chevron_up_down .glyphicon').addClass('glyphicon-chevron-up').removeClass('glyphicon-chevron-down');
        } else {
            $('.nchat_campos').hide("slow");
            $('.email_campos').hide("slow");
            $('.clicktocall_campos').show("slow");
        }
    });
    $('#formNChat').submit(function( event ) {
        event.preventDefault();
        if($('.nchat_campos').is(':visible')){
            var nome = $('#idfale_conosco_nome').val();
            var email = $('#idfale_conosco_email').val();
            var assunto = $('#idfale_conosco_assunto').val();
            var fila = $('#idfale_conosco_fila').val();
            var telefone = $('#idfale_conosco_telefone').val();
            if(nome == "" || nome.length < 3){
                $('.btnEntrar').prop('disabled', false);
                $('#idfale_conosco_nome').addClass('input_error');
                $('#idfale_conosco_nome').parents('.form-group').find('span').fadeIn("slow");
                $('#idfale_conosco_nome').parents('.form-group').find('small').addClass('small_error');
                $('#idfale_conosco_nome').focus();
                return false;
            } else {
                $('#idfale_conosco_nome').removeClass('input_error');
                $('#idfale_conosco_nome').parents('.form-group').find('span').fadeOut("slow");
                $('#idfale_conosco_nome').parents('.form-group').find('small').removeClass('small_error');
        }
        if(!/^([\w\-]+\.)*[\w\- ]+@([\w\- ]+\.)+([\w\-]{2,3})$/.test(email) || email == ""){
            $('.btnEntrar').prop('disabled', false);
            $('#idfale_conosco_email').addClass('input_error');
            $('#idfale_conosco_email').parents('.form-group').find('span').fadeIn("slow");
            $('#idfale_conosco_email').parents('.form-group').find('small').addClass('small_error');
            $('#idfale_conosco_email').focus();
            return false;
        } else {
            $('#idfale_conosco_email').removeClass('input_error');
            $('#idfale_conosco_email').parents('.form-group').find('span').fadeOut("slow");
            $('#idfale_conosco_email').parents('.form-group').find('small').removeClass('small_error');
        }
        if(assunto == "" || assunto.length < 3){
            $('.btnEntrar').prop('disabled', false);
            $('#idfale_conosco_assunto').addClass('input_error');
            $('#idfale_conosco_assunto').parents('.form-group').find('span').fadeIn("slow");
            $('#idfale_conosco_assunto').parents('.form-group').find('small').addClass('small_error');
            $('#idfale_conosco_assunto').focus();
            return false;
        } else {
            $('#idfale_conosco_assunto').removeClass('input_error');
            $('#idfale_conosco_assunto').parents('.form-group').find('span').fadeOut("slow");
            $('#idfale_conosco_assunto').parents('.form-group').find('small').removeClass('small_error');
        }
        if(fila == ""){
            $('.btnEntrar').prop('disabled', false);
            $('#idfale_conosco_fila').addClass('input_error');
            $('#idfale_conosco_fila').parents('.form-group').find('span').fadeIn("slow");
            $('#idfale_conosco_fila').parents('.form-group').find('small').addClass('small_error');
            $('#idfale_conosco_fila').focus();
            return false;
        } else {
            $('#idfale_conosco_fila').removeClass('input_error');
            $('#idfale_conosco_fila').parents('.form-group').find('span').fadeOut("slow");
            $('#idfale_conosco_fila').parents('.form-group').find('small').removeClass('small_error');
        }
        if(telefone != ""){
            var valor = $('#idfale_conosco_telefone').val();
            var tamanho = valor.length;
            var numero = valor.charAt(5);
            var ddd = valor.charAt(1);
            if(ddd == 0){
                $('#idfale_conosco_telefone').addClass('input_error');
                $('#idfale_conosco_telefone').parents('.form-group').find('span').fadeIn("slow");
                $('#idfale_conosco_telefone').parents('.form-group').find('span').text('Por favor, informe um DDD válido.');
                return false;
            } else if(tamanho == 14){
                if(numero == 9 || numero == 8 || numero == 7 || numero == 6){
                    $('#idfale_conosco_telefone').addClass('input_error');
                    $('#idfale_conosco_telefone').parents('.form-group').find('span').fadeIn("slow");
                    $('#idfale_conosco_telefone').parents('.form-group').find('span').text('Telefones celulares devem contér o 9° dígito.');
                    return false;
            } else if (numero == 0 || numero == 1) {
                $('#idfale_conosco_telefone').addClass('input_error');
                $('#idfale_conosco_telefone').parents('.form-group').find('span').fadeIn("slow");
                $('#idfale_conosco_telefone').parents('.form-group').find('span').text('Por favor, informe um número fixo/celular válido.');
                return false;
            } else {
                $('#idfale_conosco_telefone').removeClass('input_error');
                $('#idfale_conosco_telefone').parents('.form-group').find('span').fadeOut("slow");
            }
        } else if (tamanho == 15) {
            if(numero != 9){
                console.log(numero);
                $('#idfale_conosco_telefone').addClass('input_error');
                $('#idfale_conosco_telefone').parents('.form-group').find('span').fadeIn("slow");
                $('#idfale_conosco_telefone').parents('.form-group').find('span').text('Telefones celulares devem contér o 9° dígito.');
                return false;
            } else {
                $('#idfale_conosco_telefone').removeClass('input_error');
                $('#idfale_conosco_telefone').parents('.form-group').find('span').fadeOut("slow");
            }
        } else if(tamanho < 14) {
            $('#idfale_conosco_telefone').addClass('input_error');
            $('#idfale_conosco_telefone').parents('.form-group').find('span').fadeIn("slow");
            $('#idfale_conosco_telefone').parents('.form-group').find('span').text('Por favor, informe um número fixo/celular válido.');
            return false;
        } 
    } else {
        $('#idfale_conosco_telefone').removeClass('input_error');
        $('#idfale_conosco_telefone').parents('.form-group').find('span').fadeOut("slow");
    }
    sessionStorage.clear();
    const cliente = {
        id: null,
        name: nome,
        rede: 'chat',
        email: email,
        fila: 'suporte',
        assunto: assunto
    };
    sessionStorage.setItem('cliente',JSON.stringify(cliente));
    location.href = 'http://189.58.139.204:3001';
  } else if ($('.email_campos').is(':visible')){
    var nome = $('#idfale_conosco_nome_email').val();
    var email = $('#idfale_conosco_email_email').val();
    var assunto = $('#idfale_conosco_assunto_email').val();
    var mensagem = $('#idfale_conosco_mensagem_email').val();
    if(nome == "" || nome.length < 3){
        $('.btnEntrar').prop('disabled', false);
        $('#idfale_conosco_nome_email').addClass('input_error');
        $('#idfale_conosco_nome_email').parents('.form-group').find('span').fadeIn("slow");
        $('#idfale_conosco_nome_email').parents('.form-group').find('small').addClass('small_error');
        $('#idfale_conosco_nome_email').focus();
        return false;
    } else {
        $('#idfale_conosco_nome_email').removeClass('input_error');
        $('#idfale_conosco_nome_email').parents('.form-group').find('span').fadeOut("slow");
        $('#idfale_conosco_nome_email').parents('.form-group').find('small').removeClass('small_error');
    }
    if(!/^([\w\-]+\.)*[\w\- ]+@([\w\- ]+\.)+([\w\-]{2,3})$/.test(email) || email == ""){
        $('.btnEntrar').prop('disabled', false);
        $('#idfale_conosco_email_email').addClass('input_error');
        $('#idfale_conosco_email_email').parents('.form-group').find('span').fadeIn("slow");
        $('#idfale_conosco_email_email').parents('.form-group').find('small').addClass('small_error');
        $('#idfale_conosco_email_email').focus();
        return false;
    } else {
        $('#idfale_conosco_email_email').removeClass('input_error');
        $('#idfale_conosco_email_email').parents('.form-group').find('span').fadeOut("slow");
        $('#idfale_conosco_email_email').parents('.form-group').find('small').removeClass('small_error');
    }
    if(assunto == "" || assunto.length < 3){
        $('.btnEntrar').prop('disabled', false);
        $('#idfale_conosco_assunto_email').addClass('input_error');
        $('#idfale_conosco_assunto_email').parents('.form-group').find('span').fadeIn("slow");
        $('#idfale_conosco_assunto_email').parents('.form-group').find('small').addClass('small_error');
        $('#idfale_conosco_assunto_email').focus();
        return false;
    } else {
        $('#idfale_conosco_assunto_email').removeClass('input_error');
        $('#idfale_conosco_assunto_email').parents('.form-group').find('span').fadeOut("slow");
        $('#idfale_conosco_assunto_email').parents('.form-group').find('small').removeClass('small_error');
    }
    if(mensagem == "" || mensagem.length < 3){
        $('.btnEntrar').prop('disabled', false);
        $('#idfale_conosco_mensagem_email').addClass('input_error');
        $('#idfale_conosco_mensagem_email').parents('.form-group').find('span').fadeIn("slow");
        $('#idfale_conosco_mensagem_email').parents('.form-group').find('small').addClass('small_error');
        $('#idfale_conosco_mensagem_email').focus();
        return false;
    } else {
        $('#idfale_conosco_mensagem_email').removeClass('input_error');
        $('#idfale_conosco_mensagem_email').parents('.form-group').find('span').fadeOut("slow");
        $('#idfale_conosco_mensagem_email').parents('.form-group').find('small').removeClass('small_error');
    }
    sessionStorage.clear();
    const cliente = {
        id: null,
        name: nome,
        rede: 'chat',
        email: email,
        fila: 'suporte',
        assunto: assunto
    };
    sessionStorage.setItem('cliente',JSON.stringify(cliente));
    location.href = 'http://189.58.139.204:3001';
  } else if($('.clicktocall_campos').is(':visible')) {
        var nome = $('#idfale_conosco_nome_clicktocall').val();
        var telefone = $('#idfale_conosco_telefone_clicktocall').val();
        if(nome == "" || nome.length < 3){
        $('.btnEntrar').prop('disabled', false);
        $('#idfale_conosco_nome_clicktocall').addClass('input_error');
        $('#idfale_conosco_nome_clicktocall').parents('.form-group').find('span').fadeIn("slow");
        $('#idfale_conosco_nome_clicktocall').parents('.form-group').find('small').addClass('small_error');
        $('#idfale_conosco_nome_clicktocall').focus();
        return false;
    } else {
        $('#idfale_conosco_nome_clicktocall').removeClass('input_error');
        $('#idfale_conosco_nome_clicktocall').parents('.form-group').find('span').fadeOut("slow");
        $('#idfale_conosco_nome_clicktocall').parents('.form-group').find('small').removeClass('small_error');
    }
    if(telefone != ""){
        var valor = $('#idfale_conosco_telefone_clicktocall').val();
        var tamanho = valor.length;
        var numero = valor.charAt(5);
        var ddd = valor.charAt(1);
        if(ddd == 0){
            $('#idfale_conosco_telefone_clicktocall').addClass('input_error');
            $('#idfale_conosco_telefone_clicktocall').parents('.form-group').find('span').fadeIn("slow");
            $('#idfale_conosco_telefone_clicktocall').parents('.form-group').find('span').text('Por favor, informe um DDD válido.');
            return false;
        } else if(tamanho == 14){
            if(numero == 9 || numero == 8 || numero == 7 || numero == 6){
                $('#idfale_conosco_telefone_clicktocall').addClass('input_error');
                $('#idfale_conosco_telefone_clicktocall').parents('.form-group').find('span').fadeIn("slow");
                $('#idfale_conosco_telefone_clicktocall').parents('.form-group').find('span').text('Telefones celulares devem contér o 9° dígito.');
                return false;
            } else if (numero == 0 || numero == 1) {
                $('#idfale_conosco_telefone_clicktocall').addClass('input_error');
                $('#idfale_conosco_telefone_clicktocall').parents('.form-group').find('span').fadeIn("slow");
                $('#idfale_conosco_telefone_clicktocall').parents('.form-group').find('span').text('Por favor, informe um número fixo/celular válido.');
                return false;
            } else {
               $('#idfale_conosco_telefone_clicktocall').removeClass('input_error');
               $('#idfale_conosco_telefone_clicktocall').parents('.form-group').find('span').fadeOut("slow");
            }
        } else if (tamanho == 15) {
            if(numero != 9){
                $('#idfale_conosco_telefone_clicktocall').addClass('input_error');
                $('#idfale_conosco_telefone_clicktocall').parents('.form-group').find('span').fadeIn("slow");
                $('#idfale_conosco_telefone_clicktocall').parents('.form-group').find('span').text('Telefones celulares devem contér o 9° dígito.');
                return false;
            } else {
                $('#idfale_conosco_telefone_clicktocall').removeClass('input_error');
                $('#idfale_conosco_telefone_clicktocall').parents('.form-group').find('span').fadeOut("slow");
            }
        } else if(tamanho < 14) {
            $('#idfale_conosco_telefone_clicktocall').addClass('input_error');
            $('#idfale_conosco_telefone_clicktocall').parents('.form-group').find('span').fadeIn("slow");
            $('#idfale_conosco_telefone_clicktocall').parents('.form-group').find('span').text('Por favor, informe um número fixo/celular válido.');
            return false;
        } 
    } else {
        $('#idfale_conosco_telefone_clicktocall').addClass('input_error');
        $('#idfale_conosco_telefone_clicktocall').parents('.form-group').find('span').fadeIn("slow");
        $('#idfale_conosco_telefone_clicktocall').parents('.form-group').find('span').text('Por favor, informe um número fixo/celular válido.');
        return false;
    }
    sessionStorage.clear();
    const cliente = {
        id: null,
        name: nome,
        rede: 'chat',
        email: email,
        fila: 'suporte',
        assunto: assunto
    };
    sessionStorage.setItem('cliente',JSON.stringify(cliente));
    location.href = 'http://189.58.139.204:3001';
  }
  if($('.email_campos').is(':visible')) {
    var height_fale_conosco = $('.chat_nexcore').height();
    var total_bottom = height_fale_conosco - 100;
    document.getElementById("chat_nexcore").style.bottom = "-" + total_bottom + "px";
    $('.chat_nexcore').removeClass('chat_nexcore_toggle');
    $('.fale_conosco').removeClass('fale_conosco_toggle');
    $('.header_fale_conosco').removeClass('header_fale_conosco_toggle');
    swal({
        title: "Solicitação de Atendimento via Email.",
        text: "Sua solicitação de atendimento foi cadastrada. Em breve você receberá um email, cheque a sua caixa de entrada. ",
        showCancelButton: false,
        confirmButtonColor: '#e5e5e5',
        confirmButtonClass: "btn_finalizar",
        confirmButtonText: 'Finalizar',
        width: '700px',
    });
  } else if($('.clicktocall_campos').is(':visible')) {
    var height_fale_conosco = $('.chat_nexcore').height();
    var total_bottom = height_fale_conosco - 80;
    document.getElementById("chat_nexcore").style.bottom = "-" + total_bottom + "px";
    $('.chat_nexcore').removeClass('chat_nexcore_toggle');
    $('.fale_conosco').removeClass('fale_conosco_toggle');
    $('.header_fale_conosco').removeClass('header_fale_conosco_toggle');
    swal({
        title: "Solicitação de Atendimento Realizada!.",
        text: "Sua solicitação de atendimento foi enviada com sucesso. Entraremos em contato em breve. ",
        showCancelButton: false,
        confirmButtonColor: '#e5e5e5',
        confirmButtonClass: "btn_finalizar",
        confirmButtonText: 'Finalizar',
        width: '600px',
    });
  }
});
    $('#idfale_conosco_nome').on('keyup', function() {
        var valor_input = $(this).val();
        if(valor_input == "") {
        $('#idfale_conosco_nome').addClass('input_error');
        $('#idfale_conosco_nome').parents('.form-group').find('span').fadeIn("slow");
        $('#idfale_conosco_nome').parents('.form-group').find('small').addClass('small_error');
        } else {
        $('#idfale_conosco_nome').removeClass('input_error');
        $('#idfale_conosco_nome').parents('.form-group').find('span').fadeOut("slow");
        $('#idfale_conosco_nome').parents('.form-group').find('small').removeClass('small_error');
        }
    });
    $('#idfale_conosco_email').on('keyup', function() {
        var valor_input = $(this).val();
        if(!/^([\w\-]+\.)*[\w\- ]+@([\w\- ]+\.)+([\w\-]{2,3})$/.test(valor_input) || valor_input == "") {
        $('#idfale_conosco_email').addClass('input_error');
        $('#idfale_conosco_email').parents('.form-group').find('span').fadeIn("slow");
        $('#idfale_conosco_email').parents('.form-group').find('small').addClass('small_error');
        } else {
        $('#idfale_conosco_email').removeClass('input_error');
        $('#idfale_conosco_email').parents('.form-group').find('span').fadeOut("slow");
        $('#idfale_conosco_email').parents('.form-group').find('small').removeClass('small_error');
        }
    });
    $('#idfale_conosco_assunto').on('keyup', function() {
        var valor_input = $(this).val();
        if(valor_input == "") {
        $('#idfale_conosco_assunto').addClass('input_error');
        $('#idfale_conosco_assunto').parents('.form-group').find('span').fadeIn("slow");
        $('#idfale_conosco_assunto').parents('.form-group').find('small').addClass('small_error');
        } else {
        $('#idfale_conosco_assunto').removeClass('input_error');
        $('#idfale_conosco_assunto').parents('.form-group').find('span').fadeOut("slow");
        $('#idfale_conosco_assunto').parents('.form-group').find('small').removeClass('small_error');
        }
    });
    $('#idfale_conosco_fila').on('change', function() {
        var valor_input = $(this).val();
        if(valor_input == "") {
        $('#idfale_conosco_fila').addClass('input_error');
        $('#idfale_conosco_fila').parents('.form-group').find('span').fadeIn("slow");
        $('#idfale_conosco_fila').parents('.form-group').find('small').addClass('small_error');
        } else {
        $('#idfale_conosco_fila').removeClass('input_error');
        $('#idfale_conosco_fila').parents('.form-group').find('span').fadeOut("slow");
        $('#idfale_conosco_fila').parents('.form-group').find('small').removeClass('small_error');
        }
    });
    $('#idfale_conosco_nome_email').on('keyup', function() {
        var valor_input = $(this).val();
        if(valor_input == "") {
        $('#idfale_conosco_nome_email').addClass('input_error');
        $('#idfale_conosco_nome_email').parents('.form-group').find('span').fadeIn("slow");
        $('#idfale_conosco_nome_email').parents('.form-group').find('small').addClass('small_error');
        } else {
        $('#idfale_conosco_nome_email').removeClass('input_error');
        $('#idfale_conosco_nome_email').parents('.form-group').find('span').fadeOut("slow");
        $('#idfale_conosco_nome_email').parents('.form-group').find('small').removeClass('small_error');
        }
    });
    $('#idfale_conosco_email_email').on('keyup', function() {
        var valor_input = $(this).val();
        if(!/^([\w\-]+\.)*[\w\- ]+@([\w\- ]+\.)+([\w\-]{2,3})$/.test(valor_input) || valor_input == "") {
        $('#idfale_conosco_email_email').addClass('input_error');
        $('#idfale_conosco_email_email').parents('.form-group').find('span').fadeIn("slow");
        $('#idfale_conosco_email_email').parents('.form-group').find('small').addClass('small_error');
        } else {
        $('#idfale_conosco_email_email').removeClass('input_error');
        $('#idfale_conosco_email_email').parents('.form-group').find('span').fadeOut("slow");
        $('#idfale_conosco_email_email').parents('.form-group').find('small').removeClass('small_error');
        }
    });
    $('#idfale_conosco_assunto_email').on('keyup', function() {
        var valor_input = $(this).val();
        if(valor_input == "") {
        $('#idfale_conosco_assunto_email').addClass('input_error');
        $('#idfale_conosco_assunto_email').parents('.form-group').find('span').fadeIn("slow");
        $('#idfale_conosco_assunto_email').parents('.form-group').find('small').addClass('small_error');
        } else {
        $('#idfale_conosco_assunto_email').removeClass('input_error');
        $('#idfale_conosco_assunto_email').parents('.form-group').find('span').fadeOut("slow");
        $('#idfale_conosco_assunto_email').parents('.form-group').find('small').removeClass('small_error');
        }
    });
    $('#idfale_conosco_mensagem_email').on('keyup', function() {
        var valor_input = $(this).val();
        if(valor_input == "") {
        $('#idfale_conosco_mensagem_email').addClass('input_error');
        $('#idfale_conosco_mensagem_email').parents('.form-group').find('span').fadeIn("slow");
        $('#idfale_conosco_mensagem_email').parents('.form-group').find('small').addClass('small_error');
        } else {
        $('#idfale_conosco_mensagem_email').removeClass('input_error');
        $('#idfale_conosco_mensagem_email').parents('.form-group').find('span').fadeOut("slow");
        $('#idfale_conosco_mensagem_email').parents('.form-group').find('small').removeClass('small_error');
        }
    });
    $('#idfale_conosco_nome_clicktocall').on('keyup', function() {
        var valor_input = $(this).val();
        if(valor_input == "") {
        $('#idfale_conosco_nome_clicktocall').addClass('input_error');
        $('#idfale_conosco_nome_clicktocall').parents('.form-group').find('span').fadeIn("slow");
        $('#idfale_conosco_nome_clicktocall').parents('.form-group').find('small').addClass('small_error');
        } else {
        $('#idfale_conosco_nome_clicktocall').removeClass('input_error');
        $('#idfale_conosco_nome_clicktocall').parents('.form-group').find('span').fadeOut("slow");
        $('#idfale_conosco_nome_clicktocall').parents('.form-group').find('small').removeClass('small_error');
        }
    });
    $('#idfale_conosco_telefone_clicktocall').on('keyup', function() {
        var valor_input = $(this).val();
        if(valor_input == "") {
        $('#idfale_conosco_telefone_clicktocall').addClass('input_error');
        $('#idfale_conosco_telefone_clicktocall').parents('.form-group').find('span').fadeIn("slow");
        $('#idfale_conosco_telefone_clicktocall').parents('.form-group').find('small').addClass('small_error');
        } else {
        $('#idfale_conosco_telefone_clicktocall').removeClass('input_error');
        $('#idfale_conosco_telefone_clicktocall').parents('.form-group').find('span').fadeOut("slow");
        $('#idfale_conosco_telefone_clicktocall').parents('.form-group').find('small').removeClass('small_error');
        }
    });
});