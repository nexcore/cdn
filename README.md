* Esse projeto mantém os arquivos estáticos utilizados pelo embed.
Arquivos:
    - css
    - images
    - js

* Para a utilização dos arquivos desse cdn deve ser convertido os links "raw" nessa ferramenta: https://raw.githack.com/

* Após converter os arquivos escolher entre o link de desenvolvimento ou de produção, o de desenvolvimento tem uma atualização mais rápida, ou seja, em minutos, o de produção é atualizado após horas, ou seja, quando realizar um commit em algum arquivo desse cdn e o mesmo já esteja sendo utilizado, espere alguns minutos até dar um refresh na página que está utilizando esse embed para verificar a atualização.